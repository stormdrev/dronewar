// Fill out your copyright notice in the Description page of Project Settings.


#include "KillAllTurretsGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "BasePawn.h"
#include "DroneWarPlayerController.h"

void AKillAllTurretsGameMode::BeginPlay()
{
	Super::BeginPlay();
	HandleGameStart();
}

void AKillAllTurretsGameMode::HandleGameStart()
{
	DroneWarPlayerController = Cast<ADroneWarPlayerController>(UGameplayStatics::GetPlayerController(this,0));

	if (DroneWarPlayerController)
	{
		DroneWarPlayerController->SetPlayerEnabledState(false);

		FTimerHandle PlayerEnableTimerHandle;
		FTimerDelegate PlayerEnableTimerDelegate = FTimerDelegate::CreateUObject(
			DroneWarPlayerController, &ADroneWarPlayerController::SetPlayerEnabledState, true);
		GetWorldTimerManager().SetTimer(PlayerEnableTimerHandle, PlayerEnableTimerDelegate, StartDelay, false);
	}

	GamePawn = Cast<ABasePawn>(UGameplayStatics::GetPlayerPawn(this, 0));

	if (GamePawn)
	{
		GamePawn->GetHealthComponent()->OnHealthZero.AddDynamic(this, &AKillAllTurretsGameMode::HandlePlayerDeath);
	}
	GameStart();
}

void AKillAllTurretsGameMode::HandlePlayerDeath()
{
	GameOver(false);
}
