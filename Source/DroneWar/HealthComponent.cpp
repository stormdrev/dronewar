// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "KillAllTurretsGameMode.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	Health = MaxHealth;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::DamageTaken);
}

float UHealthComponent::GetHealthPercent() const
{
	return Health/MaxHealth;
}

void UHealthComponent::ChangeHealth(float HealthToAdd)
{
	Health += HealthToAdd;

	OnHealthChanged.Broadcast(GetHealthPercent());
}

void UHealthComponent::DamageTaken(AActor* DamageActor, float Damage, const UDamageType* DamageType, class AController* Instigator, AActor* DamageCauser)
{
	if (Damage <= 0.f) return;

	//Health -= Damage;
	ChangeHealth(-Damage);
	if (Health <= 0.f)
	{
		OnHealthZero.Broadcast();
	}	
	//UE_LOG(LogTemp, Warning, TEXT("Health: %f"), Health);
}
