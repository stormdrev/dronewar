// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KillAllTurretsGameMode.generated.h"

/**
 * 
 */
UCLASS()
class DRONEWAR_API AKillAllTurretsGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void GameStart();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void GameOver(bool bWonGame);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class ABasePawn* GamePawn;

private:
	class ADroneWarPlayerController* DroneWarPlayerController;

	float StartDelay = 3.f;

	void HandleGameStart();

	UFUNCTION()
	void HandlePlayerDeath();
	
};
